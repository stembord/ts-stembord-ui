import { Record } from "immutable";
import { Channel, Socket } from "phoenix";
import { ICurrentUser } from "../stores/currentUser/definitions";

class Sockets {
  socket: Socket = null as any;
  channel: Channel = null as any;

  connect(currentUser: Record<ICurrentUser>) {
    this.socket = new Socket(`${window.env.APP_WS_URL}/socket`, {
      params: {
        token: currentUser.get("token").get("key")
      }
    });

    return new Promise<Record<ICurrentUser>>((resolve, reject) => {
      this.socket.onOpen(() => {
        this.channel = this.socket.channel(`user:${currentUser.get("id")}`);
        this.channel.join();
        resolve(currentUser);
      });
      this.socket.onError(reject);

      this.socket.connect();
    });
  }

  disconnect() {
    this.socket.disconnect();
  }
}

export const sockets = new Sockets();
