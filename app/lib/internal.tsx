import { default as React } from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { Root } from "../components/lib/Root";

export const renderApp = () => {
  const rootNode = document.getElementById("root") as Element;
  unmountComponentAtNode(rootNode);

  if (process.env.NODE_ENV !== "production") {
    console.log("Mounting Root Component");
  }

  render(<Root />, rootNode);
};
