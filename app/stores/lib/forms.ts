import { createFormsStore } from "@aicacia/state-forms";
import { Consumer, state } from "../../lib/state";

export const {
  create,
  remove,
  selectForm,
  selectFormExists,
  selectField,
  selectErrors,
  selectFieldErrors,
  addError,
  addFieldError,
  updateField,
  changeField,
  removeField,
  store,
  injectForm
} = createFormsStore(state, Consumer);
