import { fromJS, Map } from "immutable";
import { state } from "../../../lib/state";
import { IRouterContext, IRouterJSON, Router, STORE_NAME } from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: IRouterJSON) =>
  Router({
    state: json.state,
    nextUrl: json.nextUrl ? fromJS(json.nextUrl) : undefined,
    url: fromJS(json.url),
    context: fromContext(json.context)
  });

export const fromContext = (context: IRouterContext): Map<string, any> => {
  const url: any = context.url || {};

  return Map({
    params: fromJS(context.params),
    url: Map({
      auth: url.auth,
      hash: url.hash,
      host: url.host,
      hostname: url.hostname,
      href: url.href,
      path: url.path,
      pathname: url.pathname,
      port: url.port,
      protocol: url.protocol,
      query: fromJS(url.query || {}),
      search: url.search,
      slashes: url.slashes
    })
  });
};

export const set = (context: IRouterContext): void => {
  store.updateState(state =>
    state.set("nextUrl", fromContext(context).get("url"))
  );
};

export const update = (context: IRouterContext, routeState: string): void => {
  const stateContext = fromContext(context);

  store.updateState(state => {
    const nextUrl = state.get("nextUrl", undefined);

    return state
      .set("state", routeState)
      .set("context", stateContext)
      .set("url", nextUrl);
  });
};
