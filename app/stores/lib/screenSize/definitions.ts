import { Record } from "immutable";

export interface IScreenSize {
  width: number;
  height: number;
}

export const ScreenSize = Record<IScreenSize>({
  width: window.innerWidth,
  height: window.innerHeight
});

export type IScreenSizeJSON = ReturnType<Record<IScreenSize>["toJS"]>;

export const STORE_NAME = "screenSize";
export const INITIAL_STATE = ScreenSize();
export const TIMEOUT = 250;
