import * as cookies from "es-cookie";
import { fromJS } from "immutable";
import { state } from "../../../lib/state";
import {
  DEFAULT_LOCALE,
  ILocalesJSON,
  ISupporttedLocale,
  loadLocale,
  LOCALE_TOKEN,
  Locales,
  STORE_NAME
} from "./definitions";

export const store = state.getStore(STORE_NAME);

store.fromJSON = (json: ILocalesJSON) =>
  Locales({
    locale: json.locale,
    messages: fromJS(json.messages)
  });

export const setLocale = (nextLocale: ISupporttedLocale) => {
  const state = store.getState(),
    currentLocale = state.get("locale");

  if (
    nextLocale === currentLocale &&
    state.get("messages").get(currentLocale)
  ) {
    return Promise.resolve(state.get("messages").get(currentLocale));
  } else {
    return loadLocale(nextLocale).then(results => {
      cookies.set(LOCALE_TOKEN, nextLocale);

      store.updateState(state =>
        state
          .set("locale", nextLocale)
          .set("messages", state.get("messages").set(nextLocale, results))
      );

      return results;
    });
  }
};

export const reloadLocale = () =>
  setLocale(store.getState().get("locale", DEFAULT_LOCALE));
