import { Error404Page } from "./components/pages/Error404/Error404Page";
import { Error500Page } from "./components/pages/Error500/Error500Page";
import { IndexPage } from "./components/pages/Index/IndexPage";
import { PrivacyPolicyPage } from "./components/pages/PrivacyPolicy/PrivacyPolicyPage";
import { TermsPage } from "./components/pages/Terms/TermsPage";
import { renderApp } from "./lib/internal";
import { router } from "./lib/router";

const init = () => {
  router.page("Error404", "/404", Error404Page);
  router.page("Error500", "/500", Error500Page);

  /* DO NOT REMOVE THIS */
  router.page("Index", "/", IndexPage);

  router.page("PrivacyPolicy", "/privacy_policy", PrivacyPolicyPage);
  router.page("Terms", "/terms", TermsPage);
};

init();

if (process.env.NODE_ENV !== "production") {
  if ((module as any).hot) {
    ((module as any).hot as any).accept(() => {
      router.clear();
      init();
      renderApp();
    });
  }
}
