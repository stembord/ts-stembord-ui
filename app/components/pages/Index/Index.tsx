import { default as React } from "react";
import { Container } from "reactstrap";
import { connect } from "../../../lib/state";
import { Layout } from "../../shared/Layout";

interface IIndexStateProps {}
interface IIndexFunctionProps {}
interface IIndexImplProps extends IIndexStateProps, IIndexFunctionProps {}

export interface IIndexProps {}
export interface IIndexState {}

const IndexConnect = connect<
  IIndexStateProps,
  IIndexFunctionProps,
  IIndexProps
>(
  (state, ownProps) => ({}),
  (state, ownProps, stateProps) => ({})
);

class IndexImpl extends React.PureComponent<IIndexImplProps, IIndexState> {
  render() {
    return (
      <Layout>
        <Container />
      </Layout>
    );
  }
}

export const Index = IndexConnect(IndexImpl);
