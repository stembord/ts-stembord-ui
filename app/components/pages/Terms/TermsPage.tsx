import { default as React } from "react";
import { Helmet } from "react-helmet";
import { injectIntl } from "react-intl";
import { Async } from "../../lib/Async";
import { JSError } from "../../lib/JSError";
import { Loading } from "../../lib/Loading";

export const TermsPage = injectIntl(({ intl }) => (
  <Async
    promise={import("./Terms")}
    onSuccess={({ Terms }) => (
      <div>
        <Helmet>
          <title>{intl.formatMessage({ id: "app.page.terms" })}</title>
        </Helmet>
        <Terms />
      </div>
    )}
    onError={error => <JSError error={error} />}
    onPending={() => <Loading />}
  />
));
