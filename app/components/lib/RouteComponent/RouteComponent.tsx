import { default as React } from "react";
import { getComponent } from "../../../lib/router";
import { connect } from "../../../lib/state";
import { selectState } from "../../../stores/lib/router";
import { Loading } from "../Loading";

interface IRouteComponentStateProps {
  Component?: React.ComponentType;
}

interface IRouteComponentFunctionProps {}

interface IRouteComponentImplProps
  extends IRouteComponentStateProps,
    IRouteComponentFunctionProps {}

export interface IRouteComponentProps {}

export interface IRouteComponentState {}

const RouteComponentConnect = connect<
  IRouteComponentStateProps,
  IRouteComponentFunctionProps,
  IRouteComponentProps
>(
  (state, ownProps) => ({
    Component: getComponent(selectState(state))
  }),
  (state, ownProps, stateProps) => ({})
);

class RouteComponentImpl extends React.PureComponent<
  IRouteComponentImplProps,
  IRouteComponentState
> {
  render() {
    const { Component } = this.props;
    return Component ? <Component /> : <Loading />;
  }
}

export const RouteComponent = RouteComponentConnect(RouteComponentImpl);
