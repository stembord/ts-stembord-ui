import { Map } from "immutable";
import { default as React } from "react";
import { IntlProvider } from "react-intl";
import { connect } from "../../../lib/state";
import {
  reloadLocale,
  selectLocale,
  selectMessages
} from "../../../stores/lib/locales";
import { Loading } from "../Loading";

interface ILocalesStateProps {
  locale: string;
  messages: Map<string, string> | null;
}
interface ILocalesFunctionProps {}
interface ILocalesImplProps extends ILocalesStateProps, ILocalesFunctionProps {}

export interface ILocalesProps {}
export interface ILocalesState {}

const LocalesConnect = connect<
  ILocalesStateProps,
  ILocalesFunctionProps,
  ILocalesProps
>(
  state => ({
    locale: selectLocale(state),
    messages: selectMessages(state)
  }),
  (state, props, owneProps) => ({})
);

class LocalesImpl extends React.PureComponent<
  ILocalesImplProps,
  ILocalesState
> {
  componentDidMount() {
    reloadLocale();
  }
  componentDidUpdate(prev: ILocalesImplProps) {
    if (prev.locale !== this.props.locale) {
      reloadLocale();
    }
  }
  render() {
    if (!this.props.messages) {
      return <Loading />;
    } else {
      return (
        <IntlProvider
          locale={this.props.locale}
          messages={this.props.messages.toJS()}
        >
          {this.props.children}
        </IntlProvider>
      );
    }
  }
}

export const Locales = LocalesConnect(LocalesImpl);
