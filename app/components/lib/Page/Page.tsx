import { default as React } from "react";
import ReactGA from "react-ga";

export interface IPageProps {}
export interface IPageState {}

export class Page extends React.PureComponent<IPageProps, IPageState> {
  componentDidMount() {
    ReactGA.pageview(window.location.pathname + window.location.search);
  }

  render() {
    return this.props.children;
  }
}
