import classNames from "classnames";
import { default as React } from "react";
import Transition, {
  TransitionProps,
  TransitionStatus
} from "react-transition-group/Transition";

export interface ISidebarProps extends Partial<TransitionProps> {
  tag?: string;
  isOpen?: boolean;
  width?: number;
  className?: string;
  navbar?: boolean;
  innerRef?: React.Ref<any>;
}

export class Sidebar extends React.Component<ISidebarProps> {
  static defaultProps: Partial<ISidebarProps> = {
    ...(Transition as any).defaultProps,
    tag: "div",
    isOpen: false,
    width: 320,
    timeout: 250
  };

  render() {
    const {
      tag,
      isOpen,
      className,
      navbar,
      children,
      timeout,
      style,
      width,
      ...transitionProps
    } = this.props;
    const Tag = tag as any;

    return (
      <Transition {...transitionProps} timeout={timeout as number} in={isOpen}>
        {status => (
          <Tag
            style={{
              ...style,
              transition: `margin-left ${timeout}ms ease-in-out`,
              scrollY: "scroll",
              width,
              marginLeft: this.getWidthFromStatus(status)
            }}
            className={classNames(
              className,
              navbar && "navbar-collapse",
              "h-100"
            )}
            aria-expanded={isOpen ? "true" : "false"}
          >
            {children}
          </Tag>
        )}
      </Transition>
    );
  }

  private getWidthFromStatus(status: TransitionStatus) {
    switch (status) {
      case "exited":
        return -(this.props.width as number);
      case "exiting":
        return -(this.props.width as number);
      default:
        return 0;
    }
  }
}
