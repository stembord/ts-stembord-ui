import { default as React } from "react";

export interface ISize {
  width: number;
  height: number;
}

export type ISizes<T extends string> = { [K in T]: ISize };

export interface ISizesStateProps {
  width: number;
  height: number;
}

export interface ISizesProps<T extends string> {
  refs: T[];
  width: number;
  height: number;
  children(
    sizes: ISizes<T>,
    refs: { [K in T]: React.RefObject<Element> }
  ): JSX.Element;
}
export type ISizesState<T extends string> = ISizes<T>;

export class Sizes<T extends string> extends React.PureComponent<
  ISizesProps<T>,
  ISizesState<T>
> {
  state: ISizesState<T> = {} as any;
  sizeRefs: { [K in T]: React.RefObject<Element> } = {} as any;

  constructor(props: ISizesProps<T>) {
    super(props);

    this.props.refs.forEach(key => {
      this.sizeRefs[key] = React.createRef();
      this.state[key] = {
        width: undefined as any,
        height: undefined as any
      };
    });
  }

  componentDidMount() {
    this.updateSizes(this.props);
  }

  componentDidUpdate(nextProps: ISizesProps<T>) {
    this.updateSizes(nextProps);
  }

  render() {
    return this.props.children(this.state, this.sizeRefs);
  }

  private updateSizes(props: ISizesProps<T>) {
    const nextState = ({ ...this.state } as any) as ISizesState<T>;

    props.refs.forEach(key => {
      if (!this.sizeRefs[key]) {
        this.sizeRefs[key] = React.createRef();
      }
      if (!nextState[key]) {
        nextState[key] = {
          width: undefined as any,
          height: undefined as any
        };
      }
    });

    Object.keys(this.sizeRefs).reduce((nextState, key) => {
      const sizeRef = (this.sizeRefs as any)[key] as React.RefObject<Element>,
        size = (this.state as any)[key] as ISize;

      if (sizeRef.current) {
        const { width, height } = sizeRef.current.getBoundingClientRect();

        if (size.width !== width || size.height !== height) {
          (nextState as any)[key] = { width, height };
        }
      }

      return nextState;
    }, nextState);

    this.setState(nextState);
  }
}
