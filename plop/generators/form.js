const path = require("path");

module.exports = {
  description: "React Form Component",
  prompts: [
    {
      type: "input",
      name: "page",
      message: "page to put form in"
    },
    {
      type: "input",
      name: "name",
      message: "component name"
    }
  ],
  actions: data => {
    const scope = data.page ? `pages/${data.page}` : "shared";

    return [
      {
        type: "add",
        path: `app/components/${scope}/{{name}}.tsx`,
        data: {
          backPath: path.relative(`./app/components/${scope}`, `./app`)
        },
        templateFile: "plop/templates/Component/Form.tsx.hbs"
      }
    ];
  }
};
